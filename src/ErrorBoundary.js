import React, { Component } from 'react';



class ErrorBoundary extends Component{
    constructor(){
      super()
      this.state = {
          hasError : false
      }
    }

    componentDidCatch(error, info){
      this.setState({ hasError: true})

      console.log("This is the error caught")
      console.log(info)
    }

    render(){
        if (this.state.hasError) {
          return <div> Oh no, something went wrong</div>
        }
        else {
          return this.props.children
    }
    }
}

export default ErrorBoundary