import React, { Component } from 'react';

const Book = (props) => (
          <div>
            Current Book: {props.book.name}
          </div>
  )

class App extends Component{
    constructor(){
      super()
      this.state = {
          book : { name : 'Secrets of the Javascript' },
      }
    }

    updateBook = () => {
       this.setState({ book : null })
    }

    render(){
          return (
          <div style={{textAlign: 'center'}}>
            <Book book={this.state.book} />
            <button onClick={this.updateBook}>Update</button>
          </div>
          );
    }
}

export default App