import React, { Component } from 'react';
import ErrorBoundary from './ErrorBoundary';

const Book = (props) => (
          <div>
            Current Book: {props.book.name}
          </div>
  )

class AppNew extends Component{
    constructor(){
      super()
      this.state = {
          book : { name : 'Secrets of the Javascript' },
      }
    }

    updateBook = () => {
       this.setState({ book : null })
    }

    render(){
          return (
          <div style={{textAlign: 'center'}}>

          <ErrorBoundary>
            <Book book={this.state.book} />
            <button onClick={this.updateBook}>Update</button>
          </ErrorBoundary>
          
          </div>
          );
    }
}

export default AppNew